#!/bin/bash

VER=$1
NOTEBOOK=`readlink -ev $2`

if [ -z ${VER} ]; then
    echo "CPU or GPU arg nedded!!"
    echo "Usage: "
    echo "  $0 CPU"
    echo "  $0 GPU"
    exit 1
elif [ ${VER} == "CPU" ]; then
    docker run -d --name tenf-${VER} -p 8887:8888 -p 6005:6006 -v $NOTEBOOK:/notebooks -e PASSWORD=nexell suker/tensorflow:${VER}
    sleep 3
    docker exec -d tenf-${VER} /run_tensorboard.sh
elif [ ${VER} == "GPU" ]; then
    nvidia-docker run -d --name tenf-${VER} -p 8888:8888 -p 6006:6006 -v $NOTEBOOK:/notebooks -e PASSWORD=nexell suker/tensorflow:${VER}
    sleep 3
    docker exec -d tenf-${VER} /run_tensorboard.sh
else
    echo "CPU or GPU arg needed!!"
    echo "Usage: "
    echo "  $0 CPU"
    echo "  $0 GPU"
    exit 1
fi
