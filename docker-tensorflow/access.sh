#!/bin/bash

VER=$1

if [ -z ${VER} ]; then
    echo "CPU or GPU arg nedded!!"
    exit 1
else
    docker exec -it tenf-${VER} /bin/bash
fi


