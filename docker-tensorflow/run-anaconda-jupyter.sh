#!/bin/bash

VER=$1

if [ -z ${VER} ]; then
    echo "CPU or GPU arg nedded!!"
    echo "Usage: "
    echo "  $0 CPU"
    echo "  $0 GPU"
    exit 1
elif [ ${VER} == "CPU" ]; then
    docker run --name tenf-${VER}-CONDA -v $PWD/notebook:/notebook -p 8887:8888 -p 6005:6006 suker/tensorflow:${VER}-CONDA
    sleep 3
    docker exec -d tenf-${VER} /run_tensorboard.sh    
elif [ ${VER} == "GPU" ]; then
    DOCKER_NVIDIA_DEVICES="--device /dev/nvidia0:/dev/nvidia0 --device /dev/nvidiactl:/dev/nvidiactl --device /dev/nvidia-uvm:/dev/nvidia-uvm"
    docker run --name tenf-${VER}-CONDA -v $PWD/notebook:/notebook -p 8888:8888 -p 6006:6006 -d $DOCKER_NVIDIA_DEVICES suker/tensorflow:${VER}-CONDA
    sleep 3
    docker exec -d tenf-${VER} /run_tensorboard.sh
else
    echo "CPU or GPU arg needed!!"
    echo "Usage: "
    echo "  $0 CPU"
    echo "  $0 GPU"
    exit 1
fi
