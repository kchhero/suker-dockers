#!/bin/bash

#crontab -e
# ===> below commands added
#30 0 * * 6 /home/jenkins/scripts/opengrok_src_init.sh
#30 5 * * * /home/jenkins/scripts/opengrok_src_sync.sh

set -e
argc=$#
arg1=$1
opengrok_storagePath=/home/jenkins/Opengrok_Storage
sourcePath=$opengrok_storagePath/src
rundate=`date +%Y%m%d_%H%M`

function init()
{
    rm -rf $sourcePath
    mkdir -p $sourcePath
}

#bl1
function bl1()
{
    cd $sourcePath
    mkdir bl1
    cd bl1

    git clone https://kchhero@review.gerrithub.io/a/NexellCorp/bl1_s5p4418
    git clone https://kchhero@review.gerrithub.io/a/NexellCorp/bl1_s5p6818

    cd $sourcePath/bl1/bl1-s5p4418
    git checkout nexell

    touch latest_init_time-$rundate


    cd $sourcePath/bl1/bl1-s5p6818
    git checkout nexell

    touch latest_init_time-$rundate
}

#kernel
function kernel()
{
    cd $sourcePath

    git clone https://kchhero@review.gerrithub.io/a/NexellCorp/linux_kernel-4.4.x
    cd $sourcePath/linux_kernel-4.4.x
    git checkout nexell

    touch latest_init_time-$rundate
}

#u-boot
function uboot()
{
    cd $sourcePath

    git clone https://kchhero@review.gerrithub.io/a/NexellCorp/u-boot-2016.01

    cd $sourcePath/u-boot-2016.01
    git checkout nexell

    touch latest_init_time-$rundate
}

#Android
function android_nougat()
{
    mkdir -p $sourcePath/android_nougat
    cd $sourcePath/android_nougat

    repo init -u https://kchhero@review.gerrithub.io/a/NexellCorp/android_manifest -b nougat

    repo sync

    touch latest_init_time-$rundate
}

#yocto

init
bl1
kernel
uboot
android_nougat
#yocto()

~/scripts/docker_opengrok_restart.sh
