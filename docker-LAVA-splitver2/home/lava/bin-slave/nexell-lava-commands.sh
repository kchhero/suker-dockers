#!/bin/bash

set -e

CURRENT_PATH=`dirname $0`
argc=$#

COMMAND_TYPE=$1
DOWNLOAD_DIR_NAME=$2
#USB_BUS_NUM=0

function run_command()
{
    local command=${COMMAND_TYPE}
    local path=${DOWNLOAD_DIR_NAME}
    #local usb_num=${USB_BUS_NUM}

    if [ $command == 'reboot-bootloader' ]; then
	echo " " > /dev/ttyUSB0; sleep 1
	echo " " > /dev/ttyUSB0; sleep 1
        echo "root" > /dev/ttyUSB0; sleep 3
        echo "root" > /dev/ttyUSB0; sleep 3
        echo "reboot" > /dev/ttyUSB0; sleep 5
        for ((i=0;i<20;i++)); do
            sleep 0.5
            echo " " > /dev/ttyUSB0
        done
        
        echo "fast 0" > /dev/ttyUSB0; sleep 1
        echo "OKAY"
	echo "======================================="
        echo "Success, reboot-bootloader!"
        echo "======================================="
    elif [ $command == 'fastboot-download' ]; then
        sleep 1
        /opt/fileshare/$path/tools/standalone-fastboot-download.sh
        sleep 3
	echo "OKAY"
        echo "=================================="
        echo "Success, Nexell Fastboot download!"
        echo "=================================="
    elif [ $command == 'running' ]; then
	sleep 2
        echo " " > /dev/ttyUSB0
        sleep 2
        echo "Success, Nexell Ready for Test!"
    elif [ $command == 'boot-on-uboot' ]; then
	echo " " > /dev/ttyUSB0; sleep 1
        echo "reset" > /dev/ttyUSB0; sleep 15
	echo "root" > /dev/ttyUSB0; sleep 2
	echo " " > /dev/ttyUSB0; sleep 2
	echo "/usr/bin/start_adbd.sh" > /dev/ttyUSB0; sleep 2
	echo " " > /dev/ttyUSB0
	echo "OKAY"
        echo "======================================="
        echo "Success, reset after fastboot download!"
        echo "======================================="
    elif [ $command == 'run-uboot-by-usb' ]; then
        echo "reset" > /dev/ttyUSB0; sleep 3
	/opt/fileshare/$path/tools/standalone-uboot-by-usb-download.sh
	for ((i=0;i<3;i++)); do
            sleep 0.5
            echo " " > /dev/ttyUSB0
        done
	echo "OKAY"
        echo "==================================="
        echo "Success, Nexell run-uboot download!"
        echo "==================================="
    else
	echo "finished"
    fi
}

run_command
