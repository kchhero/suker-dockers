#!/bin/bash

sudo docker run -it --name con_lava_slave \
     -p 69:69 -p 80:80 -p 5555:5555 -p 5556:5556 \
     -e LAVA_MASTER='192.168.1.18' -e LAVA_SERVER_IP='192.168.1.18' \
     -v /lib/modules:/lib/modules \
     -v /boot:/boot -v /dev:/dev -v /dev/bus/usb:/dev/bus/usb \
     -v ~/.ssh/id_rsa_lava.pub:/home/lava/.ssh/authorized_keys:ro \
     -h lava-slave --privileged suker/lava-slave
