#!/bin/bash

set -e

CURRENT_PATH=`dirname $0`
SCRIPT_PATH=`readlink -ev $CURRENT_PATH`

argc=$#
TARGET_NAME=$1

#declare -a targets=("s5p4418-avn-ref" "s5p4418-navi-ref" "s5p6818-artik710-raptor" "s5p6818-avn-ref" "s5p4418-smart-voice")


function select_submit()
{
    if [ "s5p4418-avn-ref-tiny" == ${TARGET_NAME} ]; then
        cd ${SCRIPT_PATH}/lavatest
        ./submit-nexell-testjob-s5p4418-avn-ref-tiny.sh
    else
        echo "TBD"
    fi
}

select_submit
