#!/bin/bash

CACHE=$1
CACHE_OPTION=

if [ -z ${CACHE} ]; then
    echo "Default build cache use!"
elif [ ${CACHE} == "no" ]; then
    echo "Default build cache use!"
elif [ ${CACHE} == "yes" ]; then
    CACHE_OPTION="--no-cache"
else
    echo "Error!!"
fi

docker build ${CACHE_OPTION} -t nexell/fileserver-base .
docker build ${CACHE_OPTION} -t nexelldocker/fileserver-releases -f deploy_releases/Dockerfile ./deploy_releases/.
docker build ${CACHE_OPTION} -t nexelldocker/fileserver-snapshot -f deploy_snapshot/Dockerfile ./deploy_snapshot/.
