#!/bin/bash

tools_path="${tools_path:-/home/lava/bin}"
cd ${tools_path}

echo "Submit v2/pipeline s5p4418-navi-ref test"
./submityaml.py -p -k apikey.txt s5p4418-navi-ref-tiny_job_normal.yaml
./submityaml.py -p -k apikey.txt s5p4418-navi-ref-tiny_job_normal_test.yaml
