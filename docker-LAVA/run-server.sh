#!/bin/bash

docker run -it --name con_lava_server -v /boot:/boot -v /lib/modules:/lib/modules -v ~/LAVA-SERVER/fileshare:/opt/fileshare -p 8000:80 -h lava-server -v /sys/fs/cgroup:/sys/fs/cgroup suker/lava-docker
