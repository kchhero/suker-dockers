#!/bin/bash

docker cp con_lava:/etc/lava-server/dispatcher-config/device-types/s5p4418.jinja2 ./etc/lava-server/dispatcher-config/device-types/

docker cp con_lava:/home/lava/lava-dispatcher/lava_dispatcher/pipeline/devices/s5p4418-01.yaml ./home/lava/lava-dispatcher/lava_dispatcher/pipeline/devices/

docker cp con_lava:/home/lava/lava-dispatcher/lava_dispatcher/pipeline/device_types/s5p4418.conf ./home/lava/lava-dispatcher/lava_dispatcher/pipeline/device_types/

docker cp con_lava:/home/lava/lava-server/lava_scheduler_app/tests/devices/s5p4418-01.jinja2 ./home/lava/lava-server/lava_scheduler_app/tests/devices/

docker cp con_lava:/home/lava/lava-server/lava_scheduler_app/tests/device-types/s5p4418.jinja2 ./home/lava/lava-server/lava_scheduler_app/tests/device-types/

docker cp con_lava:/usr/lib/python2.7/dist-packages/lava_dispatcher/pipeline/actions/deploy/fastboot.py usr/lib/python2.7/dist-packages/lava_dispatcher/pipeline/actions/deploy/

docker cp con_lava:/usr/lib/python2.7/dist-packages/lava_dispatcher/pipeline/protocols/lxc.py usr/lib/python2.7/dist-packages/lava_dispatcher/pipeline/protocols/

