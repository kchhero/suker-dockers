#!/bin/bash

docker run -it --name con_lava -v /boot:/boot -v /lib/modules:/lib/modules -v /home/suker/sukerBB/suker-dockers/docker-LAVA/fileshare:/opt/fileshare -v /dev/bus/usb:/dev/bus/usb -v ~/.ssh/id_rsa_lava.pub:/home/lava/.ssh/authorized_keys:ro --device=/dev/ttyUSB0 -p 8000:80 -p 2022:22 -h lava-slave --privileged -v /sys/fs/cgroup:/sys/fs/cgroup suker/lava-docker
