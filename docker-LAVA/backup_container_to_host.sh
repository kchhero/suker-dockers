#!/bin/bash

declare -a backuplist=(
	"/etc/lava-server/dispatcher-config/device-types/s5p4418.jinja2" \
	"/home/lava/bin/nexell-lava-commands.sh" \
	"/home/lava/bin/submit-nexell-testjob.sh" \
        "/home/lava/bin/s5p4418-navi-ref-tiny_job_normal.yaml" \
        "/home/lava/bin/s5p4418-navi-ref-tiny_job_normal_test.yaml" \
	"/home/lava/lava-server/lava_scheduler_app/tests/devices/s5p4418-01.jinja2" \
	"/usr/lib/python2.7/dist-packages/lava_dispatcher/pipeline/actions/deploy/apply_overlay.py" \
        "/usr/lib/python2.7/dist-packages/lava_dispatcher/pipeline/actions/deploy/fastboot.py" \
        "/usr/lib/python2.7/dist-packages/lava_dispatcher/pipeline/actions/deploy/lxc.py" \
        "/usr/lib/python2.7/dist-packages/lava_dispatcher/pipeline/actions/boot/fastboot.py" \
        "/usr/lib/python2.7/dist-packages/lava_dispatcher/pipeline/actions/boot/lxc.py" \
	"/usr/lib/python2.7/dist-packages/lava_dispatcher/pipeline/connections/lxc.py" \
	"/usr/lib/python2.7/dist-packages/lava_dispatcher/pipeline/connections/telnet.py" \
        "/usr/lib/python2.7/dist-packages/lava_dispatcher/pipeline/protocols/lxc.py" \
	"/usr/lib/python2.7/dist-packages/lava_dispatcher/pipeline/utils/constants.py" \
	)

for i in ${backuplist[@]}
do
    docker cp con_lava_lxc:$i .$i
done
