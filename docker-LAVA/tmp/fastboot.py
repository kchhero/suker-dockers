# Copyright (C) 2015 Linaro Limited
#
# Author: Senthil Kumaran S <senthil.kumaran@linaro.org>
#
# This file is part of LAVA Dispatcher.
#
# LAVA Dispatcher is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# LAVA Dispatcher is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along
# with this program; if not, see <http://www.gnu.org/licenses>.

from lava_dispatcher.pipeline.logical import Deployment
from lava_dispatcher.pipeline.connections.serial import ConnectDevice
from lava_dispatcher.pipeline.power import ResetDevice
from lava_dispatcher.pipeline.action import (
    Pipeline,
    JobError,
)
from lava_dispatcher.pipeline.actions.deploy import DeployAction
from lava_dispatcher.pipeline.actions.deploy.lxc import LxcAddDeviceAction
from lava_dispatcher.pipeline.actions.deploy.overlay import OverlayAction
from lava_dispatcher.pipeline.actions.deploy.download import (
    DownloaderAction,
)
from lava_dispatcher.pipeline.utils.filesystem import mkdtemp, copy_to_lxc
from lava_dispatcher.pipeline.utils.constants import (
    DISPATCHER_DOWNLOAD_DIR,
    FASTBOOT_REBOOT_TIMEOUT,
)


def fastboot_accept(device, parameters):
    """
    Each fastboot deployment strategy uses these checks
    as a base, then makes the final decision on the
    style of fastboot deployment.
    """
    if 'to' not in parameters:
        return False
    if parameters['to'] != 'fastboot':
        return False
    if not device:
        return False
    if 'actions' not in device:
        raise RuntimeError("Invalid device configuration")
    if 'deploy' not in device['actions']:
        return False
    if 'adb_serial_number' not in device:
        return False
    if 'fastboot_serial_number' not in device:
        return False
    if 'methods' not in device['actions']['deploy']:
        raise RuntimeError("Device misconfiguration")
    return True


class Fastboot(Deployment):
    """
    Strategy class for a fastboot deployment.
    Downloads the relevant parts, copies to the locations using fastboot.
    """
    compatibility = 1

    def __init__(self, parent, parameters):
        super(Fastboot, self).__init__(parent)
        self.action = FastbootAction()
        self.action.section = self.action_type
        self.action.job = self.job
        parent.add_action(self.action, parameters)

    @classmethod
    def accepts(cls, device, parameters):
        if not fastboot_accept(device, parameters):
            return False
        if 'fastboot' in device['actions']['deploy']['methods']:
            return True
        return False


class FastbootAction(DeployAction):  # pylint:disable=too-many-instance-attributes

    def __init__(self):
        super(FastbootAction, self).__init__()
        self.name = "fastboot-deploy"
        self.description = "download files and deploy using fastboot"
        self.summary = "fastboot deployment"
        self.fastboot_dir = DISPATCHER_DOWNLOAD_DIR
        try:
            self.fastboot_dir = mkdtemp(basedir=DISPATCHER_DOWNLOAD_DIR)
        except OSError:
            pass

    def validate(self):
        super(FastbootAction, self).validate()
        lava_test_results_dir = self.parameters['deployment_data']['lava_test_results_dir']
        lava_test_results_dir = lava_test_results_dir % self.job.job_id
        self.data['lava_test_results_dir'] = lava_test_results_dir
        namespace = self.parameters.get('namespace', None)
        if namespace:
            self.action_namespaces.append(namespace)
            self.set_common_data(namespace, 'lava_test_results_dir',
                                 lava_test_results_dir)
            lava_test_sh_cmd = self.parameters['deployment_data']['lava_test_sh_cmd']
            self.set_common_data(namespace, 'lava_test_sh_cmd',
                                 lava_test_sh_cmd)

    def populate(self, parameters):
        #self.internal_pipeline = Pipeline(parent=self, job=self.job, parameters=parameters)
        #self.internal_pipeline.add_action(OverlayAction())
        #if hasattr(self.job.device, 'power_state'):
        #    if self.job.device.power_state in ['on', 'off']:
        #        self.internal_pipeline.add_action(ConnectDevice())
        #        self.internal_pipeline.add_action(ResetDevice())
        #self.internal_pipeline.add_action(EnterFastbootAction())
        #self.internal_pipeline.add_action(LxcAddDeviceAction())
        image_keys = list(parameters['images'].keys())
        if 'boot' in image_keys:
            download = DownloaderAction('boot', self.fastboot_dir)
            download.max_retries = 3  # overridden by failure_retry in the parameters, if set.
            self.internal_pipeline.add_action(download)
            self.internal_pipeline.add_action(ApplyBootAction())
        if 'nexell_ext' in image_keys:          
            self.internal_pipeline.add_action(ApplyNexellAction())


class ApplyNexellAction(DeployAction):
    """
    Fastboot deploy ptable image.
    """

    def __init__(self):
        super(ApplyNexellAction, self).__init__()
        self.name = "fastboot_apply_nexell_action"
        self.description = "fastboot apply nexell image"
        self.summary = "fastboot apply nexell"
        self.retries = 3
        self.sleep = 10

    def validate(self):
        super(ApplyNexellAction, self).validate()

    def run(self, connection, args=None):
        connection = super(ApplyNexellAction, self).run(connection, args)
        fastboot_cmd1 = ['cd', '/opt/fileshare']
        fastboot_cmd2 = ['./tools/download.sh']
        command_output = self.run_command(fastboot_cmd1)
        command_output2 = self.run_command(fastboot_cmd2)
        
        if command_output1 and 'error' in command_output1:
            raise JobError("Unable to apply ptable image using fastboot: %s" %
                           command_output1)  # FIXME: JobError needs a unit test
        if command_output2 and 'error' in command_output2:
            raise JobError("Unable to apply ptable image using fastboot: %s" %
                           command_output2)  # FIXME: JobError needs a unit test
        return connection


class ApplyBootAction(DeployAction):
    """
    Fastboot deploy boot image.
    """

    def __init__(self):
        super(ApplyBootAction, self).__init__()
        self.name = "fastboot_apply_boot_action"
        self.description = "fastboot apply boot image"
        self.summary = "fastboot apply boot"
        self.retries = 3
        self.sleep = 10

    def validate(self):
        super(ApplyBootAction, self).validate()
        if 'download_action' not in self.data:
            raise RuntimeError("download-action missing: %s" % self.name)
        if 'file' not in self.data['download_action']['boot']:
            self.errors = "no file specified for fastboot boot image"
        if 'fastboot_serial_number' not in self.job.device:
            self.errors = "device fastboot serial number missing"
            if self.job.device['fastboot_serial_number'] == '0000000000':
                self.errors = "device fastboot serial number unset"

    def run(self, connection, args=None):
        connection = super(ApplyBootAction, self).run(connection, args)
        serial_number = self.job.device['fastboot_serial_number']
        lxc_name = self.get_common_data('lxc', 'name')
        src = self.data['download_action']['boot']['file']
        dst = copy_to_lxc(lxc_name, src)
        fastboot_cmd = ['lxc-attach', '-n', lxc_name, '--', 'fastboot',
                        '-s', serial_number, 'flash', 'boot', dst]
        command_output = self.run_command(fastboot_cmd)
        if command_output and 'error' in command_output:
            raise JobError("Unable to apply boot image using fastboot: %s" %
                           command_output)  # FIXME: JobError needs a unit test
        return connection
