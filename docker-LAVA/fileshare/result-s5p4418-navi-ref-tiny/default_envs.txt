bootdelay=0
baudrate=115200
fdt_high=0xffffffff
kerneladdr=0x40008000
kernel_file=zImage
fdtaddr=0x49000000
load_fdt=if test -z "$fdtfile"; then loop=$board_rev; number=$board_rev: success=0; until test $loop -eq 0 || test $success -ne 0; do if test $loop -lt 10; then number=0$loop; else number=$loop; fi; ext4load mmc $rootdev:$bootpart $fdtaddr s5p4418-navi_ref-rev${number}.dtb && setexpr success 1; setexpr loop $loop - 1; done; if test $success -eq 0; then ext4load mmc $rootdev:$bootpart $fdtaddr s5p4418-navi_ref-rev00.dtb;fi; else ext4load mmc $rootdev:$bootpart $fdtaddr $fdtfile; fi; 
rootdev=0
bootpart=1
bootargs=console=ttyAMA3,115200n8 root=/dev/mmcblk0p3 rw rootfstype=ext4 loglevel=4 rootwait quiet printk.time=1 consoleblank=0 systemd.log_level=info systemd.show_status=false
boot_cmd_mmcboot=check_hw;ext4load mmc ${rootdev}:${bootpart} $kerneladdr $kernel_file;run load_fdt;bootz $kerneladdr - $fdtaddr
mmcboot=run boot_cmd_mmcboot
bootcmd=run mmcboot


